%global mpi_list mpich openmpi
%global so_version_310 310
%global so_version_311 311

# open it when upstream support
%ifarch aarch64
%bcond_with float16
%else
%bcond_without float16
%endif

%bcond_with java

Summary: A general purpose library and file format for storing scientific data
Name: hdf5
Version: 1.14.4.2
Release: 4%{?dist}
License: BSD
URL: https://portal.hdfgroup.org/display/HDF5/HDF5

%global version_main %(echo %version | cut -d. -f-2)

Source0: https://github.com/HDFGroup/hdf5/archive/refs/tags/%{name}_%{version}.tar.gz
Source1: h5comp

Patch0001:     CVE-2022-25972-Add-bounds-checking-to-avoid-Out-of-bounds-Write-for.patch
Patch5000:     build-without-static-lib.patch

BuildRequires: gcc-c++
BuildRequires: libtool openssh-clients libaec-devel gcc,
BuildRequires: time zlib-devel hostname automake
BuildRequires: krb5-devel openssl-devel
BuildRequires: gcc-gfortran

%if %{with java}
BuildRequires: java-devel
BuildRequires: javapackages-tools
BuildRequires: hamcrest
BuildRequires: junit
BuildRequires: slf4j
%endif


%description
HDF5 is a general purpose library and file format for storing scientific data.
HDF5 can store two primary objects: datasets and groups. A dataset is
essentially a multidimensional array of data elements, and a group is a
structure for organizing objects in an HDF5 file. Using these two basic
objects, one can create and store almost any kind of scientific data
structure, such as images, arrays of vectors, and structured and unstructured
grids. You can also mix and match them in HDF5 files according to your needs.


%package devel
Summary: HDF5 development files
Requires: %{name} = %{version}-%{release}
Requires: libaec-devel
Requires: zlib-devel
Requires: gcc-gfortran

%description devel
HDF5 development headers and libraries.

%if %{with java}
%package -n java-hdf5
Summary: HDF5 java library
Requires:  slf4j

%description -n java-hdf5
HDF5 java library
%endif

%package static
Summary: HDF5 static libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
HDF5 static libraries.


%package mpich
Summary: HDF5 mpich libraries
BuildRequires: mpich-devel
Provides: %{name}-mpich2 = %{version}-%{release}

%description mpich
HDF5 parallel mpich libraries


%package mpich-devel
Summary: HDF5 mpich development files
Requires: %{name}-mpich = %{version}-%{release}
Requires: libaec-devel
Requires: zlib-devel
Requires: mpich-devel
Provides: %{name}-mpich2-devel = %{version}-%{release}

%description mpich-devel
HDF5 parallel mpich development files


%package mpich-static
Summary: HDF5 mpich static libraries
Requires: %{name}-mpich-devel = %{version}-%{release}
Provides: %{name}-mpich2-static = %{version}-%{release}

%description mpich-static
HDF5 parallel mpich static libraries


%package openmpi
Summary: HDF5 openmpi libraries
BuildRequires: openmpi-devel
BuildRequires: make

%description openmpi
HDF5 parallel openmpi libraries


%package openmpi-devel
Summary: HDF5 openmpi development files
Requires: %{name}-openmpi = %{version}-%{release}
Requires: libaec-devel
Requires: zlib-devel
Requires: openmpi-devel

%description openmpi-devel
HDF5 parallel openmpi development files


%package openmpi-static
Summary: HDF5 openmpi static libraries
Requires: %{name}-openmpi-devel = %{version}-%{release}

%description openmpi-static
HDF5 parallel openmpi static libraries


%prep
%autosetup -p1 -n %{name}-%{name}_%{version}

%if %{with java}
find . ! -name junit.jar -name "*.jar" -delete
ln -s %{_javadir}/hamcrest/hamcrest.jar java/lib/hamcrest-core.jar
ln -s %{_javadir}/slf4j/api.jar java/lib/slf4j-api-1.7.25.jar
ln -s %{_javadir}/slf4j/nop.jar java/lib/ext/slf4j-nop-1.7.25.jar
ln -s %{_javadir}/slf4j/simple.jar java/lib/ext/slf4j-simple-1.7.25.jar
%endif

sed -i -e '/^STATIC_AVAILABLE=/s/=.*/=no/' */*/h5[cf]*.in
autoreconf -f -i

sed -e 's|-O -finline-functions|-O3 -finline-functions|g' -i config/gnu-flags

%build
%global _configure ../configure
%global configure_opts \\\
  --disable-silent-rules \\\
  --enable-fortran \\\
  --enable-hl \\\
  --enable-shared \\\
  --enable-mirror-vfd \\\
  --enable-hlgiftools \\\
  --with-szlib \\\
%{nil}

%if %{without float16}
%global configure_opts %{configure_opts} \\\
  --disable-nonstandard-feature-float16
%endif

export CC=gcc
export CXX=g++
export F9X=gfortran
export LDFLAGS="%{__global_ldflags} -fPIC -Wl,-z,now -Wl,--as-needed"
mkdir build
pushd build
ln -s ../configure .
%configure \
  %{configure_opts} \
  --enable-cxx \
%if %{with java}
  --enable-java \
%endif
  --with-default-plugindir=%{_libdir}/hdf5/plugin
sed -i -e 's| -shared | -Wl,--as-needed\0|g' libtool
sed -r -i 's|^prefix=/usr|prefix=%{buildroot}/usr|' java/test/junit.sh
%make_build LDFLAGS="%{__global_ldflags} -fPIC -Wl,-z,now -Wl,--as-needed"
popd

export LDFLAGS="%{__global_ldflags} -fPIC -Wl,-z,now -Wl,--as-needed"
for mpi in %{mpi_list}
do
  mkdir $mpi
  pushd $mpi
  module load mpi/$mpi-%{_arch}
  ln -s ../configure .
  %configure \
    %{configure_opts} \
    CC=mpicc CXX=mpicxx F9X=mpif90 \
    FCFLAGS="$FCFLAGS -I$MPI_FORTRAN_MOD_DIR" \
    --enable-parallel \
    --exec-prefix=%{_libdir}/$mpi \
    --libdir=%{_libdir}/$mpi/lib \
    --bindir=%{_libdir}/$mpi/bin \
    --sbindir=%{_libdir}/$mpi/sbin \
    --includedir=%{_includedir}/$mpi-%{_arch} \
    --datarootdir=%{_libdir}/$mpi/share \
    --mandir=%{_libdir}/$mpi/share/man \
    --with-default-plugindir=%{_libdir}/$mpi/hdf5/plugin
  sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool
  %make_build LDFLAGS="%{__global_ldflags} -fPIC -Wl,-z,now -Wl,--as-needed"
  module purge
  popd
done


%install
%make_install -C build
rm %{buildroot}%{_libdir}/*.la
mkdir -p %{buildroot}%{_fmoddir}
mv %{buildroot}%{_includedir}/*.mod %{buildroot}%{_fmoddir}
sed -i -e 's,%{_includedir},%{_fmoddir},' %{buildroot}%{_bindir}/h5fc
mkdir -p %{buildroot}%{_libdir}/hdf5/plugin
for mpi in %{mpi_list}
do
  module load mpi/$mpi-%{_arch}
  %make_install -C $mpi
  rm %{buildroot}/%{_libdir}/$mpi/lib/*.la
  mkdir -p %{buildroot}${MPI_FORTRAN_MOD_DIR}
  mv %{buildroot}%{_includedir}/${mpi}-%{_arch}/*.mod %{buildroot}${MPI_FORTRAN_MOD_DIR}/
  sed -i -e "s,%{_includedir},${MPI_FORTRAN_MOD_DIR}," %{buildroot}%{_libdir}/$mpi/bin/h5pfc
  mkdir -p %{buildroot}%{_libdir}/$mpi/hdf5/plugin
  module purge
done
find %{buildroot}%{_datadir} \( -name '*.[ch]*' -o -name '*.f90' \) -exec chmod -x {} +

%ifarch x86_64
sed -i -e s/H5pubconf.h/H5pubconf-64.h/ %{buildroot}%{_includedir}/H5public.h
mv %{buildroot}%{_includedir}/H5pubconf.h \
   %{buildroot}%{_includedir}/H5pubconf-64.h
for x in h5c++ h5cc h5fc
do
  mv %{buildroot}%{_bindir}/${x} \
     %{buildroot}%{_bindir}/${x}-64
  install -m 0755 %SOURCE1 %{buildroot}%{_bindir}/${x}
done
%else
sed -i -e s/H5pubconf.h/H5pubconf-32.h/ %{buildroot}%{_includedir}/H5public.h
mv %{buildroot}%{_includedir}/H5pubconf.h \
   %{buildroot}%{_includedir}/H5pubconf-32.h
for x in h5c++ h5cc h5fc
do
  mv %{buildroot}%{_bindir}/${x} \
     %{buildroot}%{_bindir}/${x}-32
  install -m 0755 %SOURCE1 %{buildroot}%{_bindir}/${x}
done
%endif
mkdir -p %{buildroot}%{rpmmacrodir}
cat > %{buildroot}%{rpmmacrodir}/macros.hdf5 <<EOF
%%_hdf5_version %{version}
EOF

%if %{with java}
mkdir -p %{buildroot}%{_libdir}/%{name}
mv %{buildroot}%{_libdir}/libhdf5_java.so %{buildroot}%{_libdir}/%{name}/
%endif

%check
make -C build check

%files
%license COPYING
%doc release_docs/
%{_bindir}/gif2h5
%{_bindir}/h52gif
%{_bindir}/h5clear
%{_bindir}/h5copy
%{_bindir}/h5debug
%{_bindir}/h5delete
%{_bindir}/h5diff
%{_bindir}/h5dump
%{_bindir}/h5fuse
%{_bindir}/h5format_convert
%{_bindir}/h5import
%{_bindir}/h5jam
%{_bindir}/h5ls
%{_bindir}/h5mkgrp
%{_bindir}/h5perf_serial
%{_bindir}/h5repack
%{_bindir}/h5repart
%{_bindir}/h5stat
%{_bindir}/h5unjam
%{_bindir}/h5watch
%{_bindir}/mirror_server
%{_bindir}/mirror_server_stop
%{_libdir}/hdf5/
%{_libdir}/libhdf5.so.*
%{_libdir}/libhdf5_cpp.so.%{so_version_310}*
%{_libdir}/libhdf5_fortran.so.*
%{_libdir}/libhdf5hl_fortran.so.%{so_version_310}*
%{_libdir}/libhdf5_hl.so.%{so_version_310}*
%{_libdir}/libhdf5_hl_cpp.so.%{so_version_310}*

%files devel
%{_bindir}/h5c++*
%{_bindir}/h5cc*
%{_bindir}/h5fc*
%{_bindir}/h5redeploy
%{_includedir}/*.h
%{_includedir}/H5config_f.inc
%{_libdir}/*.so
%{_libdir}/*.settings
%{_fmoddir}/*.mod
%{rpmmacrodir}/macros.hdf5
%{_datadir}/hdf5_examples/

%files static
%{_libdir}/*.a

%if %{with java}
%files -n java-hdf5
%{_jnidir}/hdf5.jar
%{_libdir}/%{name}/
%endif

%files mpich
%license COPYING
%doc release_docs/
%{_libdir}/mpich/bin/gif2h5
%{_libdir}/mpich/bin/h52gif
%{_libdir}/mpich/bin/h5clear
%{_libdir}/mpich/bin/h5copy
%{_libdir}/mpich/bin/h5debug
%{_libdir}/mpich/bin/h5delete
%{_libdir}/mpich/bin/h5diff
%{_libdir}/mpich/bin/h5dump
%{_libdir}/mpich/bin/h5format_convert
%{_libdir}/mpich/bin/h5fuse
%{_libdir}/mpich/bin/h5import
%{_libdir}/mpich/bin/h5jam
%{_libdir}/mpich/bin/h5ls
%{_libdir}/mpich/bin/h5mkgrp
%{_libdir}/mpich/bin/h5redeploy
%{_libdir}/mpich/bin/h5repack
%{_libdir}/mpich/bin/h5perf
%{_libdir}/mpich/bin/h5perf_serial
%{_libdir}/mpich/bin/h5repart
%{_libdir}/mpich/bin/h5stat
%{_libdir}/mpich/bin/h5unjam
%{_libdir}/mpich/bin/h5watch
%{_libdir}/mpich/bin/mirror_server
%{_libdir}/mpich/bin/mirror_server_stop
%{_libdir}/mpich/bin/ph5diff
%{_libdir}/mpich/hdf5/
%{_libdir}/mpich/lib/*.so.%{so_version_310}*
%{_libdir}/mpich/lib/*.so.%{so_version_311}*

%files mpich-devel
%{_includedir}/mpich-%{_arch}
%{_fmoddir}/mpich/*.mod
%{_libdir}/mpich/bin/h5pcc
%{_libdir}/mpich/bin/h5pfc
%{_libdir}/mpich/lib/lib*.so
%{_libdir}/mpich/lib/lib*.settings
%{_libdir}/mpich/share/hdf5_examples/

%files mpich-static
%{_libdir}/mpich/lib/*.a

%files openmpi
%license COPYING
%doc release_docs/
%{_libdir}/openmpi/bin/gif2h5
%{_libdir}/openmpi/bin/h52gif
%{_libdir}/openmpi/bin/h5clear
%{_libdir}/openmpi/bin/h5copy
%{_libdir}/openmpi/bin/h5debug
%{_libdir}/openmpi/bin/h5delete
%{_libdir}/openmpi/bin/h5diff
%{_libdir}/openmpi/bin/h5dump
%{_libdir}/openmpi/bin/h5format_convert
%{_libdir}/openmpi/bin/h5fuse
%{_libdir}/openmpi/bin/h5import
%{_libdir}/openmpi/bin/h5jam
%{_libdir}/openmpi/bin/h5ls
%{_libdir}/openmpi/bin/h5mkgrp
%{_libdir}/openmpi/bin/h5perf
%{_libdir}/openmpi/bin/h5perf_serial
%{_libdir}/openmpi/bin/h5redeploy
%{_libdir}/openmpi/bin/h5repack
%{_libdir}/openmpi/bin/h5repart
%{_libdir}/openmpi/bin/h5stat
%{_libdir}/openmpi/bin/h5unjam
%{_libdir}/openmpi/bin/h5watch
%{_libdir}/openmpi/bin/mirror_server
%{_libdir}/openmpi/bin/mirror_server_stop
%{_libdir}/openmpi/bin/ph5diff
%{_libdir}/openmpi/hdf5/
%{_libdir}/openmpi/lib/*.so.%{so_version_310}*
%{_libdir}/openmpi/lib/*.so.%{so_version_311}*

%files openmpi-devel
%{_includedir}/openmpi-%{_arch}
%{_fmoddir}/openmpi/*.mod
%{_libdir}/openmpi/bin/h5pcc
%{_libdir}/openmpi/bin/h5pfc
%{_libdir}/openmpi/lib/lib*.so
%{_libdir}/openmpi/lib/lib*.settings
%{_libdir}/openmpi/share/hdf5_examples/

%files openmpi-static
%{_libdir}/openmpi/lib/*.a


%changelog
* Tue Oct 8 2024 Shuo Wang <abushwang@tencent.com> - 1.14.4.2-4
- fix CVE-2022-25972
- Add bounds checking to avoid Out-of-bounds Write for gif2h5 (#4786)

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.14.4.2-3
- Rebuilt for loongarch release

* Thu Jun 06 2024 cunshunxia <cunshunxia@tencent.com> - 1.14.4.2-2
- h5cc tools use shared lib instead of static one to fix cgnslib build failure.

* Tue May 14 2024 Upgrade Robot <upbot@opencloudos.tech> - 1.14.4.2-1
- Upgrade to version 1.14.4.2

* Tue Mar 12 2024 Shuo Wang <abushwang@tencent.com> - 1.12.1-4
- backport patch to fix CVE-2021-37501
- Check for overflow when calculating on-disk attribute data size

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.12.1-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.12.1-2
- Rebuilt for OpenCloudOS Stream 23.05

* Wed Apr 19 2023 cunshunxia <cunshunxia@tencent.com> - 1.12.1-1
- initial build
